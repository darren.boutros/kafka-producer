package com.darren.kafka.domain;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter @RequiredArgsConstructor @NoArgsConstructor
@EqualsAndHashCode
public class Instrument implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1927377692949604338L;
	private @NonNull String isin;
	private @NonNull String finacialMarket;
	private @NonNull String currency;
	private @NonNull Long price;

}
