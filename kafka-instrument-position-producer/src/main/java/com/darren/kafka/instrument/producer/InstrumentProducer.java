package com.darren.kafka.instrument.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.darren.kafka.domain.Instrument;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class InstrumentProducer {

	@Autowired
	private KafkaTemplate<String, ?> kafkaTemplate;
	
	@Value("${kafka.topic.instrument}")
	private String topic;
	
	public void send (Instrument payload) {
		log.info("sending payload='{}' to topic='{}'", payload.toString(), topic);
		kafkaTemplate.send(MessageBuilder.withPayload(payload).setHeader(KafkaHeaders.TOPIC, topic).build());
	}
}
