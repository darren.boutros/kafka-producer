package com.darren.kafka.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.darren.kafka.domain.Instrument;
import com.darren.kafka.instrument.producer.InstrumentProducer;

@Configuration
public class ProducerConfiguration {

	@Value("${kafka.bootstrap-servers}")
	private String bootstrapServers;

	@Bean
	public ProducerFactory<String, Instrument> producerFactory(){
		return new DefaultKafkaProducerFactory<>(producerConfigs());
	}

	@Bean
    public KafkaTemplate<String, Instrument> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }
	
	@Bean
	public InstrumentProducer instrumentProducer() {
		return new InstrumentProducer();
	}
	
	private Map<String, Object> producerConfigs(){
		Map<String, Object> properties = new HashMap<>();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,  bootstrapServers);
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,JsonSerializer.class);
		return properties;
	}

}
