package com.darren.kafka;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.springframework.kafka.test.assertj.KafkaConditions.key;
import static org.springframework.kafka.test.hamcrest.KafkaMatchers.hasValue;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.junit4.SpringRunner;

import com.darren.kafka.domain.Instrument;
import com.darren.kafka.instrument.producer.InstrumentProducer;

import lombok.extern.slf4j.Slf4j;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DemoApplicationTests {

	private final static String INSTRU_TOPIC = "insturment.topic";
	
	//Automatically start an embedded Kafka broker
	@ClassRule
	public static KafkaEmbedded embeddedKafka = new KafkaEmbedded(1, true, INSTRU_TOPIC);
	
	@Autowired
	private InstrumentProducer instrumentProducer;
	
	private KafkaMessageListenerContainer<String, Instrument> container;
	
	private BlockingQueue<ConsumerRecord<String, Instrument>> records;
	
	@Test
	public void testInstrumentProducer() throws InterruptedException {
		
		Instrument instrument = new Instrument("10", "fr", "euro", 10l);
		// send the message
		instrumentProducer.send(instrument);
		
		//check that the message was received
		ConsumerRecord<String, Instrument> received = records.poll(10, TimeUnit.SECONDS);
	
		//Hamcrest Matchers to check the value;
	    assertThat(received.value()).isEqualTo(instrument);
	    
	    // AssertJ Condition to check the key
	    assertThat(received).has(key(null));
		
	}
	
	//Setup a test-listener on the topic
	@Before
	public void setUp() throws Exception {
		
		//Set up the consumer properties
		Map<String, Object> consumerProperties = KafkaTestUtils.consumerProps("sender", "false", embeddedKafka);
		
		consumerProperties.forEach((v,k)-> System.err.println(v + "::" + k));
		//Create a consumer factory
		DefaultKafkaConsumerFactory<String, Instrument> consumerFactory = new DefaultKafkaConsumerFactory<>(consumerProperties,
				new StringDeserializer(), 
				new JsonDeserializer<>(Instrument.class));
	
		
		//Set the topic that needs to be consumed
		ContainerProperties containerProperties = new ContainerProperties(INSTRU_TOPIC);
		
		//Create a MessageListenerContainer
		container = new KafkaMessageListenerContainer<>(consumerFactory, containerProperties);
		
		//Create a thread sage queue to store the received message
		records = new LinkedBlockingQueue<>();
		
		//setup a message listener
		container.setupMessageListener(new MessageListener<String, Instrument>() {

			@Override
			public void onMessage(ConsumerRecord<String, Instrument> record) {
				log.debug("received message={}", record.toString());
				records.add(record);
			}
		});
		
		//Start the container 
		container.start();
		
		//wait until the container has the required number of assigned partitions
		ContainerTestUtils.waitForAssignment(container, embeddedKafka.getPartitionsPerTopic());
		
	}
	
	//Stop the container
	@After
	public void shutdown() {
		container.stop();
	}

}
